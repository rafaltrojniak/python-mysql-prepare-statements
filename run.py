import mysql.connector
from mysql.connector import Error

connection = mysql.connector.connect(host='db',
                                     database='information_schema',
                                     user='root',
                                     password='test')

cursor = connection.cursor(prepared=True)
sql_insert_query = """ select * from plugins WHERE PLUGIN_TYPE=%s"""

insert_tuple = ('AUDIT',)

cursor.execute(sql_insert_query, insert_tuple, multi=True)

for id in cursor.fetchall():
    print(id)

